import requests
import urlparse

from crowler.models import URL


def create_url(original_url):
    url = original_url
    parced = urlparse.urlparse(original_url)
    print(parced)

    if not bool(parced.scheme):
        parced = parced._replace(**{"scheme": "http"})
        url = parced.geturl()

    try:
        r = requests.get(url.replace('///', '//'))
    except Exception, e:
        is_live = False
    else:
        is_live = r.status_code < 400

    obj, created = URL.objects.get_or_create(
        address=original_url, defaults={'is_live': is_live}
    )
    return obj
