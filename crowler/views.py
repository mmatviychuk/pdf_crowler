from rest_framework import mixins
from rest_framework import viewsets
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.viewsets import GenericViewSet

from crowler.models import Document, URL
from crowler.serializers import DocumentSerializer, URLSerializer, DocumentDetailSerializer


class DocumentViewSet(viewsets.ModelViewSet):
    parser_classes = (MultiPartParser, FormParser,)
    serializer_class = DocumentSerializer
    queryset = Document.objects.all()
    detail_serializer = DocumentDetailSerializer

    def retrieve(self, *args, **kwargs):
        self.serializer_class = self.detail_serializer
        return viewsets.ModelViewSet.retrieve(self, *args, **kwargs)


class URLViewSet(mixins.ListModelMixin, GenericViewSet):
    serializer_class = URLSerializer
    queryset = URL.objects.all()
