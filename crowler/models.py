from django.db import models


class Document(models.Model):
    name = models.CharField(max_length=256)
    datetime = models.DateTimeField(auto_now_add=True)


class URL(models.Model):
    address = models.TextField(unique=True)
    is_live = models.BooleanField(default=True)


class Connection(models.Model):
    document = models.ForeignKey(Document, related_name="connections")
    url = models.ForeignKey(URL, related_name="connections")

    class Meta:
        unique_together = ('document', 'url')
