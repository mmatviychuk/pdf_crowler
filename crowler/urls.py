from rest_framework.routers import DefaultRouter

from crowler.views import DocumentViewSet, URLViewSet

router = DefaultRouter()
router.register(r'documents', DocumentViewSet, base_name='document')
router.register(r'urls', URLViewSet, base_name='url')
urlpatterns = router.urls
