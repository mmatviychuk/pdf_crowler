from rest_framework import serializers

import pdfx
from crowler.models import Document, Connection, URL
from crowler.utils import create_url


class URLSerializer(serializers.ModelSerializer):
    doc_number = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = URL
        fields = '__all__'

    def get_doc_number(self, obj):
        return obj.connections.count()


class DocumentSerializer(serializers.ModelSerializer):
    file = serializers.FileField(required=True, write_only=True)
    name = serializers.CharField(read_only=True)
    url_number = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Document
        fields = ('id', 'file', 'name', 'url_number')

    def create(self, validated_data):
        file_name = validated_data['file'].name
        pdf = pdfx.PDFx(file_name)
        references_dict = pdf.get_references_as_dict()

        print references_dict

        urls = map(create_url, references_dict['url'])
        document = Document.objects.create(name=file_name)

        for url in urls:
            Connection.objects.create(document=document, url=url)

        return document

    def get_url_number(self, obj):
        return obj.connections.count()


class DocumentDetailSerializer(serializers.ModelSerializer):
    urls = serializers.SerializerMethodField()

    class Meta:
        model = Document
        fields = ('urls',)

    def get_urls(self, obj):
        urls = URL.objects.filter(connections__in=obj.connections.all())
        serializer = URLSerializer(urls, many=True)
        return serializer.data
