# PDF CRAWLER

Project is written on python2.7 using Django 1.10 and DRF for API creation.
For pdf file parsing pdfx library is used.
Posgresql used as DB.
Documents with the same name could be uploaded, they will be saved as separate items.

To run application locally make next steps:
1. Create postgres DB with name `crowler` and user `crowler_user`. Or change these name in settings.

2. Run in the terminal from pdf_crowler directory
```virtualenv venv
. venv/bin/activate
pip install -r requirements.txt
python ./manage.py migrate
python ./manage.py runserver
```

List of API urls:
`http://127.0.0.1:8000/api/`

Document uploading: use form for document uploading
`http://127.0.0.1:8000/api/documents/`

Documents list:
`http://127.0.0.1:8000/api/documents/`

Document review:
`http://127.0.0.1:8000/api/documents/<id>/`

Urls list:
`http://127.0.0.1:8000/api/urls/`
